# Plastic Waste detection

Plastic Waste detection is an object detection model which works on tensorflow object detection api, It is trained on faster_RCNN_resnet101_coco model over a dataset currently consisting of 45 classes.The detected objects and the corresponding counts of easch label are given as output

## Installation

copy this repository into tensorflow/models/object_detection/
.Move the file visualization_utils.py into object_detection/utils


## Usage

training the model further:

python train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/faster_rcnn_resnet101_coco.config

test the model:

use object_detection_tutorial.ipynb
.Add the test images into the test_images folder


## Output results

Sample outputs are in result images/

## Please feel free to add your own images and increase the number of classes
